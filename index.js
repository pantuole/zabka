const { stdout } = require("node:process");
const { promisify } = require("node:util");
const { exec } = require("node:child_process");
const { watch, access, constants, readdir, rm } = require("node:fs/promises");

const TESTS_FOLDER = "./ENG";
const TESTS_OPTION = "--tests";
const ALLOW_SHUFFLE = "--allow-shuffle";

const execute = promisify(exec);

const controller = new AbortController();

const colors = {
    YELLOW: "\x1b[33m%s\x1b[0m",
    CYAN: "\x1b[36m%s\x1b[0m",
    RED: "\x1b[31m%s\x1b[0m",
    GREEN: "\x1b[32m%s\x1b[0m"
};

const exists = async (name) => (
    access(name, constants.R_OK).then(() => true).catch(() => false)
)

function isInput(filename) {
    return filename.includes("_in");
}

function isShuffleError(message) {
    if (!message) {
        return false;
    }

    let totalLines = 0;
    const uniqueLines = new Set();

    for (const line of message.split("\n")) {
        if (line[0] === ">" || line[0] === "<") {
            uniqueLines.add(line.slice(2));
            totalLines++;
        }
    }

    return totalLines === uniqueLines.size * 2;
}

async function runTest(filename, index) {
    if (!isInput(filename)) {
        return Promise.resolve();
    }

    const runOutput = "./out" + index;
    const correctOutput = filename.replace("in", "out");
    try {
        await execute(`./a.out < ${TESTS_FOLDER}/${filename} > ${runOutput}`);
        await execute(`diff ${TESTS_FOLDER}/${correctOutput} ${runOutput}`);
        console.log(colors.GREEN, `${filename} success`);
    } catch (err) {
        if (this.allowShuffle && isShuffleError(err.stdout)) {
            console.log(colors.GREEN, `${filename} success (different output order)`);
        } else {
            console.log(colors.RED, `${filename} failure.`);
            stdout.write(err.stdout || err.stderr || err.toString());
        }
    } finally {
        await rm(runOutput);
    }
}

async function compileAndRun(name, options) {
    try {
        console.log(colors.YELLOW, `Compiling ${name}`);
        await execute(`g++ ${name} --pedantic -Wall -Wextra -fsanitize=address`);

        if (options.withTests) {
            console.log(colors.CYAN, "Running tests");
            const files = await readdir(TESTS_FOLDER);
            await Promise.all(files.map(runTest.bind(options)));
        } else {
            console.log(colors.CYAN, `Running ${name}`);
            const res = await execute(`./a.out`);
            stdout.write(res.stdout);
        }
    } catch (err) {
        console.error(err.stderr ?? err);
    }
}

async function main({ withTests, allowShuffle }) {
    if (allowShuffle && !withTests) {
        console.log(colors.RED, `Tests are not run`);
        return;
    }

    const [cFile, cppFile] = await Promise.all([
        exists("main.c"),
        exists("main.cpp")
    ]);

    if (!cFile && !cppFile) {
        console.warn("No main file found.");
        console.warn("Stopping script...");
        return;
    }

    const filename = cFile ? "main.c" : "main.cpp";

    try {
        const watcher = watch(filename, { signal: controller.signal });

        if (withTests && !(await exists(TESTS_FOLDER))) {
            console.log(colors.YELLOW, "No tests found");
            return;
        }

        await compileAndRun(filename, { withTests, allowShuffle });

        for await (const event of watcher) {
            await compileAndRun(event.filename, { withTests, allowShuffle });
        }
    } catch (err) {
        if (err.name !== "AbortError") throw err;
    }
}

main({
    withTests: process.argv.indexOf(TESTS_OPTION) !== -1,
    allowShuffle: process.argv.indexOf(ALLOW_SHUFFLE) !== -1
});

process.on("SIGINT", () => {
    controller.abort();
});